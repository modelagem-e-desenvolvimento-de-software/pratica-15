
import java.util.ArrayList;

/** @author messiah */

public class CareTaker {
    protected ArrayList<Memento> mementoList;
    
    public CareTaker(){
        mementoList = new ArrayList<Memento>();
    }
    
    public void add(Memento m){
        mementoList.add(m);
    }
    
    public Memento get(int i){
        System.out.print(i+"-");
        return mementoList.get(i);
    }    
}
