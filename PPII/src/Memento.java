/** @author messiah */

public class Memento {
    private final float valor;
    
   public Memento(float v){
       this.valor = v;
   }
   
   public float getValor(){
       return this.valor;
   }
}
