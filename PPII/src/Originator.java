/** @author messiah */
public class Originator {
    private float valor;
    
    public void setValor(float v){        
        this.valor = v;
    }
    
    public float getValor(){
        System.out.println("Valor corrente: " + this.valor);
        return this.valor;
    }
    
    public Memento saveValorToMemento(){
        return new Memento(this.valor);
    }
    
    public void getValorFromMemento(Memento m){
        this.valor = m.getValor();
        System.out.println("Valor salvo: "+ this.valor);
    }
}
