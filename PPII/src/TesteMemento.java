/** @author messiah  */

public class TesteMemento {

    public static void main(String[] args) {
        CareTaker care_taker = new CareTaker();
        
        Originator originator = new Originator();
        
        originator.setValor(100);
        care_taker.add(originator.saveValorToMemento());
        
        originator.setValor(200);        
        care_taker.add(originator.saveValorToMemento());
        
        originator.setValor(300);
        care_taker.add(originator.saveValorToMemento());
        
        originator.setValor(400);
        care_taker.add(originator.saveValorToMemento());
        
        originator.getValor();
        
        originator.getValorFromMemento(care_taker.get(3));
        originator.getValorFromMemento(care_taker.get(2));
        originator.getValorFromMemento(care_taker.get(1));
        originator.getValorFromMemento(care_taker.get(0));
        
    }
    
}
