package Questao4;

public class HexaObserver extends Observer{

    public HexaObserver(Subject su) {
        this.subject = su;
    }

    @Override
    public void update() {
        System.out.println("Hexa : " + Integer.toString(this.subject.getState(), 16));
    }
}
