package Questao4;

public class principal {

	public static void main(String[] args) {
		Subject su = new Subject();
		int n;
		
        Observer binario = new BinaryObserver(su);
        Observer octal = new OctalObserver(su);
        Observer hexa = new HexaObserver(su);

        su.attach(binario);
        su.attach(hexa);
        su.attach(octal);

        n = 15;
        
        System.out.println("Primeiro valor = " + n);
        su.setState(n);  
        
        n = 10;
        System.out.println("Segundo valor = " + n);
        su.setState(n);  
	}
}
