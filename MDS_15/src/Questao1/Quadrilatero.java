package Questao1;

public class Quadrilatero extends Figura {

    private Ponto p1;
    private Ponto p2;
    private Ponto p3;
    private Ponto p4;

    public Quadrilatero(String cor, Ponto p1, Ponto p2, Ponto p3, Ponto p4){
        super(cor);
    	this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }

    @Override
	public void desenha() {
			System.out.println("Desenhando um Retangulo " + this.getCor());
	}

}
