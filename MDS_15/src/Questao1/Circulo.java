package Questao1;

public class Circulo extends Figura {

    private Ponto p1;
    private Ponto p2;

    public Circulo(String cor, Ponto p1, Ponto p2){
    	super(cor);
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
	public void desenha() {
		System.out.println("Desenhando um circulo " + this.getCor());
	}
}
