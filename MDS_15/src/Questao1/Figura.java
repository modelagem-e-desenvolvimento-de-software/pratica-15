package Questao1;

public class Figura {

    protected String cor;
    
    public Figura(String cor) {
		this.cor = cor;
	}

    protected void desenha(){
    }
    
    public String getCor() {
		return cor;
	}

	protected void setCor(String cor) {
		this.cor = cor;
	}

}
