package Questao1;

public class Principal {

	public static void main(String[] args) {
		String cor = "Preto";
		Ponto p = new Ponto(10,10);
		Figura roda1 = new Circulo(cor, p, p);
		Figura roda2 = new Circulo(cor, p, p);
		Figura baixo = new Quadrilatero(cor, p, p, p, p);
		Figura cima = new Quadrilatero(cor, p, p, p, p);
		
		
		GerarCarrinho carrinho = new GerarCarrinho(roda1,roda2,baixo,cima);
		carrinho.desenha();

	}

}
