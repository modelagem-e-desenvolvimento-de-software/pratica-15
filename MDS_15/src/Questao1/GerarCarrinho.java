package Questao1;

public class GerarCarrinho {

    private Figura roda1;
    private Figura roda2;
    private Figura baixo;
    private Figura cima;

    public GerarCarrinho(Figura roda1, Figura roda2, Figura baixo, Figura cima){
        this.baixo = baixo;
        this.cima = cima;
        this.roda1 = roda1;
        this.roda2 = roda2;
    }

    public void desenha(){
        roda1.desenha();
        roda2.desenha();
        baixo.desenha();
        cima.desenha();
    }




}
