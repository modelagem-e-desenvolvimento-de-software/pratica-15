package Questao3;

public enum AbstractLogNivel {
    NO_LOG,
    INFO,
    DEBUG,
    ERROR
}
