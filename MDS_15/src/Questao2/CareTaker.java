package Questao2;

import java.util.ArrayList;


public class CareTaker {

    ArrayList<Memento> mementoList = new ArrayList<>();

    public CareTaker() {
		mementoList = new ArrayList<Memento>();
	}
    public void add (Memento e){
        mementoList.add(e);
    }
    
    public Memento get() {
		if(mementoList.size()<= 0) {
			return new Memento(-1);
		}
		Memento ultimoEstado = mementoList.get(mementoList.size() - 1);
		mementoList.remove(mementoList.size() - 1);
		return ultimoEstado;
	}

    public int getIndex() {
		return mementoList.size() - 1;
	}
}
