package Questao2;


public class Principal {

	public static void main(String[] args) {
		
		Originator ori = new Originator();
		
		ori.saveValorToMemento(100);
		ori.saveValorToMemento(100);
		ori.saveValorToMemento(100);
		ori.saveValorToMemento(100);
		
		ori.mostrarValor();
		ori.getValorFromMemento();
		ori.getValorFromMemento();
		ori.getValorFromMemento();
		ori.getValorFromMemento();
	}

}
