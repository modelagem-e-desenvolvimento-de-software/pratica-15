package Questao2;


public class Originator {
    private float valor;
    protected CareTaker caretaker;
    
    public Originator() {
		this.valor = 0;
		this.caretaker = new CareTaker();
	}

	public void setValor(float valor) {
        this.valor = valor;
    }

    public float getValor() {
        return valor;
    }

    public void saveValorToMemento(float novo) {
		caretaker.add(new Memento(valor));
		valor += novo;	
	}
		
	public void mostrarValor() {
		System.out.println("Valor corrente:" + valor);
	}

    public void getValorFromMemento(){
        System.out.println(caretaker.getIndex() + "-"+ "Valor salvo: " + caretaker.get().getValor());
		
    }

}
