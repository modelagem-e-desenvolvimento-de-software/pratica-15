package Questao2;

public class Memento {
    private float valor;

    public Memento(float valor){
        this.valor = valor;
    }

    public float getValor(){
        return valor;
    }
}
