/** @author messiah */

public abstract class AbstractLog {
    protected NIVEL level;
    AbstractLog nextLog;
    
    public enum NIVEL{
        NO_LOG, INFO, DEBUG, ERROR;
    }
    
    AbstractLog(NIVEL level){
        this.level = level;
    }
            
    public void setNextLog(AbstractLog abstractlog){
        if(this.nextLog == null){
            this.nextLog = abstractlog;
        }else{
            this.nextLog.setNextLog(abstractlog);
        }
    }
    
    public void logMessage(int i, String s){        
        System.out.println("Nivel chamado "+this.level+" Nivel executar = "+this.nextLog.level);
    }
    
    public void addLog(AbstractLog abstractlog){
        this.setNextLog(abstractlog);
    }
    
    protected abstract void write(String s);    
}
