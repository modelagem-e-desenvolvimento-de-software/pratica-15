/** @author messiah */

public class ConsoleLog extends AbstractLog{
    
    public ConsoleLog(){
        super(NIVEL.INFO);
    }

    @Override
    protected void write(String s) {
        System.out.println("Enviando uma mensagem.");
        System.out.println("---------------------------------");
    }    
}
