/** @author messiah */

public class FileLog extends AbstractLog{
    
    public FileLog(){
        super(NIVEL.DEBUG);
    }
    
    @Override
    protected void write(String s) {
        System.out.println("Console: Enviando uma informação de debug.");
        System.out.println("---------------------------------");
    }
}
