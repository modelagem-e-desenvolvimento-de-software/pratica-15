/** @author messiah */

public class ErrorLog extends AbstractLog{
    
    public ErrorLog(){ 
        super(NIVEL.ERROR);
    }

    @Override
    protected void write(String s) {
        System.out.println("Enviando um erro.");
    }    
}
