/** @author messiah */

public class GotChainResponsibility {
    AbstractLog chainOfLog;
    
    public AbstractLog getChainOfLog(){
        return chainOfLog;
    }
    
    public static void main(String[] args) {
        AbstractLog abstractlog = new ConsoleLog();
        abstractlog.setNextLog(new ErrorLog());
        abstractlog.setNextLog(new FileLog());
        try{
            abstractlog.logMessage(1, "oi");
            abstractlog.addLog(new ConsoleLog());
            abstractlog.addLog(new ErrorLog());
            abstractlog.addLog(new FileLog());
        }catch (Exception e){
            e.printStackTrace();
        }
        
    }
    
}

/*

public class TesteChain {
    public static void main(String[] args) {
        BancoChain bancos = new BancoA();
        bancos.setNext(new BancoB());
        bancos.setNext(new BancoC());
        bancos.setNext(new BancoD());
        try {
            bancos.efetuarPagamento(IDBancos.bancoC);
            bancos.efetuarPagamento(IDBancos.bancoD);
            bancos.efetuarPagamento(IDBancos.bancoA);
            bancos.efetuarPagamento(IDBancos.bancoB);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
*/