/** @author messiah  */

public class BinaryObserver extends Observer {

    public BinaryObserver(Subject su) {
        this.subject = su;
    }

    @Override
    public void update() {	
        System.out.println("Binario : " + Integer.toString(this.subject.getState(), 2));
    }
}
