
import java.util.ArrayList;

/** @author messiah  */

public class Subject {
    private ArrayList<Observer> observers = new ArrayList<Observer>();

    private int state;

    public int getState() {
        return state;
    }
    
    public void setState(int state){
        this.state = state;
        this.notifyAllObserver();
    }

    public void attach(Observer ob){
        observers.add(ob);
    }

    public void notifyAllObserver(){
        for (int i = 0 ; i < observers.size(); i++)
            	observers.get(i).update();
   }
}
