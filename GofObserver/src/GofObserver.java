/** @author messiah  */

public class GofObserver {

    public static void main(String[] args) {
        Subject su = new Subject();
	int n;
		
        Observer bin = new BinaryObserver(su);
        su.attach(bin);
        
        Observer oct = new OctalObserver(su);
        su.attach(oct);
        
        Observer hex = new HexaObserver(su);
        su.attach(hex);
                
        n = 15;        
        System.out.println("Primeiro valor = " + n);
        su.setState(n);  
        
        n = 10;
        System.out.println("Segundo valor = " + n);
        su.setState(n); 
    }
    
}
