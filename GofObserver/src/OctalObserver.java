/** @author messiah  */

public class OctalObserver extends Observer {

    public OctalObserver(Subject su) {
        this.subject = su;
    }

    @Override
    public void update() {
        System.out.println("Octal String : " +Integer.toString(this.subject.getState(), 8));
    }
}
