
import java.awt.Graphics;
import java.io.PrintStream;

/** @author messiah */

public class Quadrilatero extends Figura{
    private Ponto p1, p2, p3, p4;
    
    public Quadrilatero(Ponto p1, Ponto p2, Ponto p3, Ponto p4){
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
    }
    
    @Override
    public void desenha(Graphics g){
        g.setColor(this.cor);
        //Assinatura: drawRect(int xTopLeft, int yTopLeft, int width, int height);
        g.drawRect((int)(this.p4.getX()), (200-(int)(this.p4.getY())), (int)(this.p2.getX() - this.p1.getX()), (int)(this.p4.getY() - this.p1.getY()));
    }
    
    @Override
    public String toString(){
        return "Desenhando um quadrilatero "+Integer.toString(cor.getRGB())+".";
    }
}
