/** @author messiah */

public class Ponto {
    private double x, y;
    
    public Ponto(double x, double y){
        this.x = x;
        this.y = y;
    }
    
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
}
