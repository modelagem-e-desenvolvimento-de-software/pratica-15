
import java.awt.Graphics;

/** @author messiah  */

public class Circulo extends Figura{
    private Ponto p1, p2;
    private double raio;
            
    public Circulo(Ponto p1, Ponto p2){
        this.p1  = p1;
        this.p2 = p2;
    }
    
    @Override
    public void desenha(Graphics g){
        this.raio = (p2.getY()-p1.getY())/2;
        
        g.setColor(cor);
        g.drawOval((int)(p1.getX()-raio), (int)(200 - p2.getY()), (int)(2*raio), (int)(p2.getY()-p1.getY()));
    }
    
    @Override
    public String toString(){
        return "Desenhando um circulo "+Integer.toString(cor.getRGB())+".";
    }
}
