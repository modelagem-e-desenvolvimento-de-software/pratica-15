
import java.awt.Color;
import java.awt.Graphics;

/** @author messiah */

public class GerarCarrinho implements Forma{
    private final Figura roda1, roda2, baixo, cima;
    
    public GerarCarrinho(){
        this.roda1 = new Circulo(new Ponto(10*5, 0*5), new Ponto(10*5, 5*5));
        
        this.roda2 = new Circulo(new Ponto(30*5, 0*5), new Ponto(30*5, 5*5));
        
        this.baixo = new Quadrilatero(new Ponto(0*5, 5*5), new Ponto(40*5, 5*5), new Ponto(40*5, 25*5), new Ponto(0*5, 25*5));
        
        this.cima = new Quadrilatero(new Ponto(10*5, 25*5), new Ponto(30*5, 25*5), new Ponto(30*5, 30*5), new Ponto(10*5, 30*5));
    }    
    
    @Override
    public void desenha(Graphics g) {
        
        this.roda1.setCor(Color.BLACK);
        this.roda1.desenha(g);
        System.out.println(this.roda1);
        
        this.roda2.setCor(Color.BLACK);
        this.roda2.desenha(g);
        System.out.println(this.roda2);
        
        this.baixo.setCor(Color.BLACK);
        this.baixo.desenha(g);
        System.out.println(this.baixo);
        
        this.cima.setCor(Color.BLACK);
        this.cima.desenha(g);
        System.out.println(this.cima);
    }    
}
