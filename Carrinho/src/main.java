import javax.swing.*;
import java.awt.*;

public class main extends JPanel {

   @Override
   public void paintComponent(Graphics g) {
        GerarCarrinho gc = new GerarCarrinho();
        
        gc.desenha(g);        
   }

   @Override
   public Dimension getPreferredSize() {
      return new Dimension(250, 250);
   }

   public static void main(String args[]) {
      JFrame jf = new JFrame();
      jf.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
      jf.add(new main());
      jf.pack();
      jf.setVisible(true);
   }
}