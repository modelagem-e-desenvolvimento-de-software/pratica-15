
import java.awt.Color;
import java.awt.Graphics;

/** @author messiah */

public abstract class Figura {
    protected Color cor;
    
    protected void setCor(Color c){
        this.cor = c;
    }
    
    protected void desenha(Graphics g){}
    
}
