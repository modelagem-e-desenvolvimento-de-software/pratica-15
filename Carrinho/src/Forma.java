
import java.awt.Graphics;

/** @author messiah */

public abstract interface Forma{
    
    public void desenha(Graphics g);
}
